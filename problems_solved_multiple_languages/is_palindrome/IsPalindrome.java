class IsPalindrome {

    public static void main (String args[]) {

        System.out.println("Is a word a palíndrome?");
        String response = "YES!";

        System.out.print("Type the word: ");
        String word = System.console().readLine();

        int size = word.length();
        for (int i = 0; i < size/2; i++) {
            if (word.charAt(i) != word.charAt(size-1 - i)) {
                response = "NOT!";
                break;
            }
        }
        System.out.println(response);
    }  // end main

}  // end class

#include <iostream>
#include <string>

int main() {
  std::cout << "Is a word a palíndrome?\n";
  std::string word;
  std::string response = "YES!";

  std::cout << "Type the word: ";
  std::cin >> word;

  int size = word.length();
  for (int i = 0; i < size/2; i++) {
      if (word[i] != word[size-1 - i]) {
          response = "NOT!";
          break;
      }
  }
  std::cout << response << '\n';
}

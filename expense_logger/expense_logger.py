import sys
import time
from datetime import date

def format_value(value):
    return "{:06.2f}".format(value)

def total_of_day():
    return None
def total_spended():
    return None

file_name = 'register.txt'
num_last_line = 0
value_acumulated = 0.0
today = date.today()
week_day = date.isoweekday(today) + 1
date_of_spend = "[{}] {}".format(today, week_day)

# somar novos gastos recebidos como argumento
values_spending = sys.argv[1:]
for i in values_spending:
    value_acumulated = value_acumulated + float(i)

# abrir o arquivo e ler a primeira e a última linhas
register_file = open(file_name, 'r+')
first_line = register_file.readline(10)  # lê 10 primeiros caracteres da primeira linha
line_list = register_file.readlines()  # ler o conteúdo da última linha
last_line_content = line_list[len(line_list) - 1]

# value_acumulated = value_acumulated + float(last_line_content[-7:])
formated_value = format_value(value_acumulated)
formated_date = today.strftime('%Y-%m-%d')
if (first_line == formated_date):
    last_line_new_content = last_line_content.replace(last_line_content[-7:], formated_value + '\n')
    register_file.write(last_line_new_content)
    register_file.close()
else:
    register_file.write('\n')
    register_file.write(date_of_spend + ': R$ ' + formated_value + '\n')
    register_file.close()

    with open(file_name, 'r') as f:
        lines = f.readlines()
    lines[0] = formated_date
    with open(file_name, 'w') as f:
        f.writelines(lines)

total_of_day = "TOTAL: R$ "



